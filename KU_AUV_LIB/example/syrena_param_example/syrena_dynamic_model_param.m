clear all, close all, clc

%% ###### Thruster Parameter #######

thruster_param


%% ######  Mass and Buoyancy  ######
m = 412; % mass of AUV (kg)
g = 9.81; % gravity acceleration

W = m*g; % weight
rho = 995.6502;  % salted water density at 25 C 
volume = 0.41383; % volume (m^3)
B = rho*g*volume; %

%% ######  Center of Garvity and Buoyancy  ######
% distant from the body origin o_b to CG on each axis (m)
x_g = 0.002699;
y_g = 0.000172;
z_g = 0.003368;
r_g = [x_g y_g z_g]; %Center of gravity with body fixed frame

% distant from the body origin o_b to CB on each axis (m)
x_b = 0.0;  
y_b = 0.0;
z_b = 0.0;

r_b = [x_b y_b z_b];

%% ######  Mass and Inertia Matrix  ######

% inertia 

I_x = 21.32951846;
I_xy = 0;
I_xz = 0.22494044;
I_yx = 0;
I_y = 28.1446234;
I_yz = 0;
I_zx = 0.22494044;
I_zy = 0;
I_z = 11.61824844;

global M_RB
M_RB = [m,              0,               0,             0,          m*z_g,         -m*y_g;
        0,              m,               0,        -m*z_g,              0,          m*x_g;
        0,              0,               m,         m*y_g,         -m*x_g,              0;
        0,         -m*z_g,           m*y_g,           I_x,          -I_xy,          -I_xz;  
    m*z_g,              0,          -m*x_g,         -I_yx,            I_y,          -I_yz;
   -m*y_g,          m*x_g,               0,         -I_zx,          -I_zy,            I_z];
    
% Added mass
% Need to search

X_udot = 0;
Y_vdot = 0;
Z_wdot = 0;
K_pdot = 0;
M_qdot = 0;
N_rdot = 0;

global M_A
M_A = [X_udot   0   0   0   0   0;
        0    Y_vdot  0   0   0   0;
        0    0   Z_wdot  0   0   0;
        0    0   0   K_pdot  0   0;
        0    0   0   0   M_qdot  0;
        0    0   0   0   0   N_rdot];
    
%% #####  Drag force #####

%Linear drag
% Need to search
X_u = 0;
Y_v = 0;
Z_w = 0;
K_p = 0;
M_q = 0;
N_r = 0;
% D_l = -diag([0,0,0,0,0,0]);
global D_L
D_L = -[X_u      0       0       0       0       0;
       0        Y_v     0       0       0       0;
       0        0       Z_w     0       0       0;
       0        0       0       K_p     0       0;
       0        0       0       0       M_q     0;
       0        0       0       0       0       N_r];

%Quadratic drag
% Need to search
X_uu = 0;
Y_vv = 0;
Z_ww = 0;
K_pp = 0;
M_qq = 0;
N_rr = 0;

%%%%%%%%%%%%%%%% Colioris effect and Centripetal %%%%%%%%%%%%%%%
global D_Q
global D
 D_Q = -[X_uu      0       0       0       0       0;
        0       Y_vv     0       0       0       0;
        0        0       Z_ww     0       0       0;
        0        0       0       K_pp     0       0;
        0        0       0       0       M_qq     0;
        0        0       0       0       0       N_rr];
 
D = D_L + D_Q;